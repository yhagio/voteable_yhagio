# Simple Voteable Gem for Rails

Ruby Gem: https://rubygems.org/gems/voteable_yhagio

- Users can easily vote (up or down) anything

### Usage

i.e. `/models/post.rb`, just include
```rb
class Post < ActiveRecord::Base
  include Voteable
  ...
end
```

### Development

i.e.
```sh
gem build voteable_yhagio.gemspec
gem push voteable_yhagio-0.1.0.gem
```