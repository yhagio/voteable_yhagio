Gem::Specification.new do |s|
  s.name = "voteable_yhagio"
  s.version = "0.1.1"
  s.date = "2019-04-29"
  s.summary = "A voting gem"
  s.description = "A simple voting gem."
  s.authors = ["Yuichi Hagio"]
  s.email = "yhagio@test.com"
  s.files = ["lib/voteable_yhagio.rb"]
  # s.homepage = "https://github/yhagio/voteable_gem"
  s.licenses = ["MIT"]
end